/*
 * Copyright (c) 2013 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

#include <openssl/sha.h>
#include <openssl/md5.h>
#include <keccak/KeccakHash.h>

using namespace std;

void
Test_MD5(const char *data, size_t len)
{
    MD5_CTX state;
    unsigned char hash[MD5_DIGEST_LENGTH];

    MD5_Init(&state);
    MD5_Update(&state, data, len);
    MD5_Final(hash, &state);
}

void
Test_SHA256(const char *data, size_t len)
{
    SHA256_CTX state;
    unsigned char hash[SHA256_DIGEST_LENGTH];

    SHA256_Init(&state);
    SHA256_Update(&state, data, len);
    SHA256_Final(hash, &state);
}

void
Test_Keccak256(const char *data, size_t len)
{
    Keccak_HashInstance state;
    BitSequence hash[32];

    Keccak_HashInitialize(&state, /* rate */1344, /* cap */256, 256, 0x01);
    Keccak_HashUpdate(&state, (BitSequence *)data, len * 8);
    Keccak_HashFinal(&state, (BitSequence *)&hash);
}

void
empty_hash()
{
    char buf[5];
    SHA256_CTX state;
    Keccak_HashInstance kstate;
    unsigned char hash[SHA256_DIGEST_LENGTH];

    SHA256_Init(&state);
    SHA256_Update(&state, buf, 0);
    SHA256_Final(hash, &state);

    for (size_t i = 0; i < 32; i++) {
        printf("%02x", (int)hash[i]);
    }
    printf("\n");

    Keccak_HashInitialize(&kstate, /* rate */1344, /* cap */256, 256, 0x37);
    Keccak_HashUpdate(&kstate, (BitSequence *)buf, 0);
    Keccak_HashFinal(&kstate, (BitSequence *)&hash);

    for (size_t i = 0; i < 32; i++) {
        printf("%02x", (int)hash[i]);
    }
    printf("\n");
}

#define CHUNK_LEN   (4*1024)
#define TEST_LEN    (1024*1024*1024)

int main(int argc, const char *argv[])
{
    uint64_t len = TEST_LEN;
    char *buf = new char[len];
    struct timeval start, end;
    float tDiff;

#ifdef __FreeBSD__
    sranddev();
#endif /* __FreeBSD__ */

    for (uint64_t i = 0; i < len; i++) {
        buf[i] = rand() % 256;
    }

    /* MD5 */
    gettimeofday(&start, 0);
    for (uint64_t i = 0; i < len; i += CHUNK_LEN) {
        Test_MD5(buf + i, CHUNK_LEN);
    }
    gettimeofday(&end, 0);

    tDiff = end.tv_sec - start.tv_sec;
    tDiff += (float)(end.tv_usec - start.tv_usec) / 1000000.0;

    printf("MD5: Time %3.3f, Speed %3.2fMB/s\n", tDiff, 1024.0 / tDiff);

    /* SHA-256 */
    gettimeofday(&start, 0);
    for (uint64_t i = 0; i < len; i += CHUNK_LEN) {
        Test_SHA256(buf + i, CHUNK_LEN);
    }
    gettimeofday(&end, 0);

    tDiff = end.tv_sec - start.tv_sec;
    tDiff += (float)(end.tv_usec - start.tv_usec) / 1000000.0;

    printf("SHA-256: Time %3.3f, Speed %3.2fMB/s\n", tDiff, 1024.0 / tDiff);

    /* Keccak-256 */
    gettimeofday(&start, 0);
    for (uint64_t i = 0; i < len; i += CHUNK_LEN) {
        Test_Keccak256(buf + i, CHUNK_LEN);
    }
    gettimeofday(&end, 0);

    tDiff = end.tv_sec - start.tv_sec;
    tDiff += (float)(end.tv_usec - start.tv_usec) / 1000000.0;

    printf("Keccak-256: Time %3.3f, Speed %3.2fMB/s\n", tDiff, 1024.0 / tDiff);

    empty_hash();
}

